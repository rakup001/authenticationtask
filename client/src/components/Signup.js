import {Link } from 'react-router-dom';
import {useState} from 'react';
import axios from 'axios';
const Signup = () => {
  
    const [data,setData] = useState({
        firstName:"",
        lastName:"",
        email:"",
        password:""
    });
    const[error,setError] = useState("");
    const[sucess,setSucess] = useState("");

   
    //handle change to set form data
    const handleChange = ({currentTarget:input}) => {
        setData({...data, [input.name]: input.value });
    }

    //submit form to post data
    const handleSubmit = async (e)=>{
        e.preventDefault();
        
        try{
            
            const {data:res} =  await axios.post(`${process.env.REACT_APP_API_URL}/signup`,data);
            setSucess(res.message);
            setData({ firstName:"",
            lastName:"",
            email:"",
            password:""});
        }catch(error){
            if(error.response){
                setError(error.response.data.message);
            }
        }
    }

  return (
   
    <div className="App">
      <div className="outer">
        <div className="inner">
          <form onSubmit= {handleSubmit}>
            <h3>Register</h3>

            <div className="form-group">
              <label>First name</label>
              <input
                type="text"
                className="form-control"
                placeholder="First name"
                name="firstName"
                value={data.firstName}
                required
                onChange={handleChange}
              />
            </div>

            <div className="form-group">
              <label>Last name</label>
              <input
                type="text"
                className="form-control"
                placeholder="Last name"
                name="lastName"
                value={data.lastName}
                required
                onChange={handleChange}
              />
            </div>

            <div className="form-group">
              <label>Email</label>
              <input
                type="email"
                className="form-control"
                placeholder="Enter email"
                name="email"
                value={data.email}
                required
                onChange={handleChange}
              />
            </div>

            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                className="form-control"
                placeholder="Enter password"
                name="password"
                value={data.password}
                required
                onChange={handleChange}
              />
            </div>
            {error && <div>{error}</div>}
            {sucess &&<div>{sucess}</div> }
            <button type="submit" className="btn btn-dark btn-lg btn-block">
              Register
            </button>
            <p className="forgot-password text-right">
              Already registered <Link to="/signin">sign in</Link> 
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};
export default Signup;
