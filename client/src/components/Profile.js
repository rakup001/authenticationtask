import { Navbar, Container, Button } from "react-bootstrap";
import axios from "axios";
import { useNavigate, useLocation } from "react-router-dom";

const Profile = () => {
  const navigate = useNavigate();

  //  sign out handle
  const handleLogout = () => {
    localStorage.removeItem("jwt");

    navigate("/signin");
  };

  return (
    <Navbar>
      <Container>
        <Navbar.Brand>Profile</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            <Button variant="danger" onClick={handleLogout}>
              Sign Out
            </Button>
          </Navbar.Text>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Profile;
