import { useState } from "react";
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";
import { GoogleLogin } from "react-google-login";

const Signin = () => {
  const [data, setData] = useState({
    email: "",
    password: "",
  });
  const [error, setError] = useState("");
  const navigate = useNavigate();

  //google signin

  const clientId = process.env.REACT_APP_GOOGLE_CLIENT_ID;
  //google login success
  const onLoginSuccess = (res) => {
    localStorage.setItem("jwt", JSON.stringify(res.profileObj));
    navigate("/profile");
  };
  //on google Login Failure
  const onLoginFailure = (res) => {
    console.log("Login Failed:", res);
  };

  //handle change to set form data
  const handleChange = ({ currentTarget: input }) => {
    setData({ ...data, [input.name]: input.value });
  };

  //submit form to post data
  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const { data: res } = await axios.post(
        `${process.env.REACT_APP_API_URL}/signin`,
        data
      );

      if (typeof window !== "undefined") {
        localStorage.setItem("jwt", JSON.stringify(res));
      }
      navigate("/profile");
      setData({ email: "", password: "" });
    } catch (error) {
      if (error.response) {
        setError(error.response.data.message);
      }
    }
  };

  return (
    <div className="App">
      <div className="outer">
        <div className="inner">
          <form onSubmit={handleSubmit}>
            <h3>Log in</h3>

            <div className="form-group">
              <label>Email</label>
              <input
                type="email"
                className="form-control"
                placeholder="Enter email"
                name="email"
                value={data.email}
                required
                onChange={handleChange}
              />
            </div>

            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                className="form-control"
                placeholder="Enter password"
                name="password"
                value={data.password}
                required
                onChange={handleChange}
              />
            </div>
            {error && <div>{error}</div>}
            <button type="submit" className="btn btn-dark btn-lg btn-block">
              Sign in
            </button>
            <div className="g-login">
              <GoogleLogin
                clientId={clientId}
                buttonText="Sign In"
                onSuccess={onLoginSuccess}
                onFailure={onLoginFailure}
                cookiePolicy={"single_host_origin"}
              />
            </div>
            <p className="forgot-password text-right">
              Not registered <Link to="/">Register</Link>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Signin;
