import { Routes, Route } from "react-router-dom";
import Signup from "./components/Signup";
import Signin from "./components/Signin";
import Profile from "./components/Profile";


function App() {
  return (
    <Routes>
   
            <Route path="/" exact element = {<Signup/>} />
            
            <Route path="/signin" exact element = {<Signin/>} />
            <Route path="/profile" exact element = {<Profile/>} />
    </Routes>
  );
}

export default App;
