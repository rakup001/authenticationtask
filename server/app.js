const express =require('express')
const app = express()
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const expressValidator = require('express-validator')
const cors = require("cors");
const dotenv= require('dotenv')
dotenv.config()

//database

mongoose.connect(process.env.MONGO_URI)
.then(()=>{console.log("Database connected")})

mongoose.connection.on("error",err=>{
    console.log(`DB connection error:${err.message}`);
});

//bring routes

const authRoutes = require("./routes/auth");

//middleware
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());

//Route middleware
app.use("/",authRoutes);


//port
const port=5000;
app.listen(port, () => console.log(`Server running on port ${port}`));