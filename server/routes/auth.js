const express = require("express");
const { signup, signin,signout } = require("../controllers/auth");
const { body, validationResult } = require("express-validator");
const router = express.Router();

router.post(
  "/signup",
  body("email").isEmail().normalizeEmail(),
  body("firstName","First Name is required").notEmpty(),
  body("lastName","Last Name is required").notEmpty(),
  body("email").isEmail().normalizeEmail(),
  body("password")
  .isLength({min:6})
  .withMessage("Password must contain at least 6 character")
  .matches(/\d/)
  .withMessage("Password must contain a number"),
  (req, res,next) => {
    const errors = validationResult(req);
  
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        message: errors.array()[0].msg,
      });
    }
    next();
  },
  signup
);

router.post("/signin", signin ); 
router.get("/signout", signout)
module.exports = router;
