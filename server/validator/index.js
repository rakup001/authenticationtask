exports.userSignupValidator =  (req,res, next)=>{
    // name is not null and between 4-10 characters
    req.body("name", "Name is required").notEmpty();

    //email is not null, and valid

    req.body("email", "Email must be valid")
    .matches(/.+@+\..+/)
    .withMessage("Email mustcontain @")
    .isLength({
        min:4,
        max:200
    });


    //check for password
        req.body("password","Password is required").notEmpty();
        req.body('password')
        .isLength({min:6})
        .withMessage("Password must contain at least 6 character")
        .matches(/\d/)
        .withMessage("Password must contain a number");

    //check for errors

    const error = req.validationErrors();

    if(errors){
        const firstError = error.map(error=>error.msg)[0];
        return res.status(400).json({error:firstError});
    }

}