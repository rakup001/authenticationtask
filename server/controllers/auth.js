var jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
require("dotenv").config();

const User = require("../models/user");

exports.signup = async (req, res) => {
  //check the user
  const userExists = await User.findOne({ email: req.body.email });
  //if user exist
  if (userExists) {
    return res.status(403).json({ error: "Email is taken!" });
  }
  const salt = await bcrypt.genSalt(Number(process.env.SALT));
  const hashPassword = await bcrypt.hash(req.body.password, salt);

  const user = await new User({ ...req.body, password: hashPassword });
  await user.save();
  res.status(200).json({ message: "Signup sucess! Please login" });
};

exports.signin =  (req, res) => {
  //find the user based on email
  
  const { email, password } = req.body;
  User.findOne({ email }, async(err, user) => {
    //if error
    if (err || !user) {
      return res.status(401).json({
        error: "Invalid email or password",
      });
    }
    //if user authenticate
    const validPassword =  await bcrypt.compare(
      req.body.password,
      user.password
    );

    if(!validPassword){
        return res.status(401).send({message:"Invalid Email or Password"});
    }
    //generate the token with userid and secret
    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET);

    //persist the token as 't' in cooke with expiry date
    res.cookie("t", token, { expire: new Date() + 9999 });
    //return response with user and token to front end client
    const { _id, firstName,lastName, email } = user;
   
    return res.json({ token, user: { _id, firstName, lastName, email } });
  });
};

exports.signout = (req, res) => {
  res.clearCookie("t");
  return res.json({ message: "Signout Succesfull" });
};
